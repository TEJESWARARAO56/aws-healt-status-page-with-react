import React, { Component } from "react";
import { AiOutlineCheckCircle } from 'react-icons/ai'
import './index.css'

class ServiceStatusTable extends Component {
    constructor(props) {
        super(props);
        this.wrapperRef = React.createRef();
        this.state = {
            scrollLeft: 2000,
        };
        this.servicesData = [
            { name: 'Service 1', status: Array.from({ length: 100 }, (_, i) => i + 1) },
            { name: 'Service 2', status: Array.from({ length: 100 }, (_, i) => i + 1) },
            { name: 'Service 3', status: Array.from({ length: 100 }, (_, i) => i + 1) },
            { name: 'Service 4', status: Array.from({ length: 100 }, (_, i) => i + 1) },
            { name: 'Service 5', status: Array.from({ length: 100 }, (_, i) => i + 1) },
            { name: 'Service 6', status: Array.from({ length: 100 }, (_, i) => i + 1) },
            { name: 'Service 7', status: Array.from({ length: 100 }, (_, i) => i + 1) },
            { name: 'Service 8', status: Array.from({ length: 100 }, (_, i) => i + 1) },
            { name: 'Service 9', status: Array.from({ length: 100 }, (_, i) => i + 1) },
            { name: 'Service 10', status: Array.from({ length: 100 }, (_, i) => i + 1) },
        ];
    }

    updatedate = () => {
        this.setState({ date: this.props.date })
    }

    handleScrollLeft = () => {
        const wrapper = this.wrapperRef.current;
        // if (wrapper.scrollLeft < 950) {
        //     return null
        // }
        wrapper.scrollLeft -= 950;
        console.log(wrapper.scrollLeft)
        this.setState({ scrollLeft: wrapper.scrollLeft });
    };

    handleScrollRight = () => {
        const wrapper = this.wrapperRef.current;
        wrapper.scrollLeft += 950;
        // console.log(wrapper.scrollLeft)
        this.setState({ scrollLeft: wrapper.scrollLeft });
    };

    render() {
        let day = this.props.date
        day.setDate(day.getDate() + 1);
        let last100Days = [...Array(100)].map((_, i) => {
            day.setDate(day.getDate() - 1);
            return day.toLocaleDateString('en-US', { day: 'numeric', month: 'short' });
        });
        day.setDate(day.getDate() + 99);
        let filteredData = this.servicesData
            .filter((service) => service.name.toLowerCase().includes(this.props.searchinput.toLowerCase()))
        return (
            <>
                <div className="scroll-buttons">
                    <button onClick={this.handleScrollLeft}>{"<<"}</button>
                    <button onClick={this.handleScrollRight}>{">>"}</button>
                </div>
                <div className="table-wrapper" ref={this.wrapperRef}>
                    <table>
                        <thead>
                            <tr>
                                <th className="services-column">Services</th>
                                {last100Days.map((day, index) => (
                                    <th className='date-width'>{day}</th>
                                ))}
                            </tr>
                        </thead>
                        <tbody>
                            {filteredData.map(service => (
                                <tr key={service.name}>
                                    <td className='service-name '>{service.name}</td>
                                    {service.status.map((status, i) => (
                                        <td key={i}> <AiOutlineCheckCircle className="check-icon grid-align-center" /> </td>
                                    ))}
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </>
        );
    }
}

export default ServiceStatusTable;


