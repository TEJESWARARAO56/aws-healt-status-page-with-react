import { Component } from "react";
import Regions from '../Regions/Regions'
import ServiceStatusTable from "../ServiceStatusTable/ServiceStatusTable";
import { GrRefresh } from 'react-icons/gr'
import { AiOutlineCheckCircle } from 'react-icons/ai'
import { BiSearch } from "react-icons/bi"
import './index.css'


class ServiceHealthStatusPage extends Component {

    state = { activeregion: "Asia Pacific", searchinput: "", date: new Date() }

    changeDate = (event) => {
        this.setState({ date: new Date(event.target.value) })
    }

    changeinput = (event) => {
        this.setState({ searchinput: event.target.value })
    }

    changeactiveregion = (newregion) => {
        this.setState({ activeregion: newregion })
    }
    render() {
        const { date, activeregion, searchinput } = this.state
        let dateformat = date.toISOString().substring(0, 10)
        return (
            <div className="page">
                <div className="top-section">
                    <div className="header">
                        <div className="refresh-icon-container">
                            <p>AWS Health Dashboard</p>
                            <GrRefresh className="refresh-icon" />
                        </div>
                        <div className="title-container">
                            <div>
                                <h1>Service health</h1>
                                <p>View the current and historical status of all AWS services.</p>
                            </div>
                            <div className="open-account-health-card">
                                <h2>View your account health</h2>
                                <p>Get a personalized view of events that affect your AWS account or organization.</p>
                                <button className="open-account-health-button">Open Your account health</button>
                            </div>

                        </div>
                    </div>
                </div>

                <div className="bottom-section">
                    <div className="recent-issues-container">
                        <AiOutlineCheckCircle className="check-symbol" />
                        <h1 className="no-issues-title">No recent issues</h1>
                    </div>
                    <div className="service-history-card">
                        <h2>Service History</h2>
                        <p>The following table is a running log of AWS service
                            interruptions for the past 12 months. Choose a status icon to
                            see status updates for that service. All dates and times are
                            reported in Pacific Daylight Time (PDT). To update your
                            time zone, see Time zone settings.</p>
                        <div className="search-container">
                            <div className="search-input-container">
                                <BiSearch className="search-icon" />
                                <input type="search" className="search-input" value={searchinput}
                                    onChange={this.changeinput} placeholder="Find an AWS service or Region" />
                            </div>
                            <input type="date" onChange={this.changeDate} value={dateformat} className="date-input" />
                        </div>
                        <Regions onchangeactiveregion={this.changeactiveregion} activeregion={activeregion} />
                    </div>
                    <div className="table-container">
                        <ServiceStatusTable date={date} searchinput={searchinput} />
                    </div>
                </div>
            </div>
        )
    }
}

export default ServiceHealthStatusPage