import './index.css'

const regions = [
    "North America",
    "South America",
    "Europe",
    "Africa",
    "Asia Pacific",
    "Middle East"]
function Regions(props) {
    const { activeregion, onchangeactiveregion } = props;

    function changeactiveregion(event) {
        console.log(event.target.textContent)
        console.log(activeregion)
        onchangeactiveregion(event.target.textContent)
    }

    return (
        <div className="regions-container">
            {regions.map((region, index) => {
                let rightborder = index === regions.length - 1 ? "" : "right-border"
                let activeregionclass = activeregion === region ? "activeregionclass" : ""
                return (
                    <div key={index}>
                        <button type='button' className={`${rightborder} ${activeregionclass} button`} onClick={changeactiveregion}>{region}</button>
                    </div>
                )
            })}
        </div>
    )
}

export default Regions;